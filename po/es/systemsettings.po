# translation of systemsettings.po to Spanish
# Translation of systemsettings to Spanish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Enrique Matias Sanchez (aka Quique) <cronopios@gmail.com>, 2007.
# Jaime Robles <jaime@kde.org>, 2007, 2008, 2009.
# Eloy Cuadra <ecuadra@eloihr.net>, 2009, 2014, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: systemsettings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-19 01:56+0000\n"
"PO-Revision-Date: 2023-02-10 17:32+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"com>\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra,Jaime Robles,Enrique Matías Sánchez (Quique)"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net,jaime@kde.org,cronopios@gmail.com"

#: app/main.cpp:58 app/SettingsBase.cpp:62
#, kde-format
msgid "Info Center"
msgstr "Centro de información"

#: app/main.cpp:60
#, kde-format
msgid "Centralized and convenient overview of system information."
msgstr "Resumen centralizado y práctico de información del sistema."

#: app/main.cpp:62 app/main.cpp:73
#, kde-format
msgid "(c) 2009, Ben Cooksley"
msgstr "© 2009, Ben Cooksley"

#: app/main.cpp:69 app/SettingsBase.cpp:65 app/sidebar/qml/introPage.qml:62
#: runner/systemsettingsrunner.cpp:113
#, kde-format
msgid "System Settings"
msgstr "Preferencias del sistema"

#: app/main.cpp:71
#, kde-format
msgid "Central configuration center by KDE."
msgstr "Centro de configuración principal creado por KDE."

#: app/main.cpp:84
#, kde-format
msgid "Ben Cooksley"
msgstr "Ben Cooksley"

#: app/main.cpp:84
#, kde-format
msgid "Maintainer"
msgstr "Encargado"

#: app/main.cpp:85
#, kde-format
msgid "Marco Martin"
msgstr "Marco Martin"

#: app/main.cpp:85
#, kde-format
msgid "Author"
msgstr "Autor"

#: app/main.cpp:86
#, kde-format
msgid "Mathias Soeken"
msgstr "Mathias Soeken"

#: app/main.cpp:86
#, kde-format
msgid "Developer"
msgstr "Desarrollador"

#: app/main.cpp:87
#, kde-format
msgid "Will Stephenson"
msgstr "Will Stephenson"

#: app/main.cpp:87
#, kde-format
msgid "Internal module representation, internal module model"
msgstr "Representación de módulos internos, modelo de módulos internos"

#: app/main.cpp:95
#, kde-format
msgid "List all possible modules"
msgstr "Listar todos los módulos posibles"

#: app/main.cpp:96 app/main.cpp:157
#, kde-format
msgid "Configuration module to open"
msgstr "Módulo de configuración a abrir"

#: app/main.cpp:97 app/main.cpp:158
#, kde-format
msgid "Arguments for the module"
msgstr "Argumentos para el módulo"

#: app/main.cpp:105
#, kde-format
msgid "The following modules are available:"
msgstr "Están disponibles los siguientes módulos:"

#: app/main.cpp:123
#, kde-format
msgid "No description available"
msgstr "Descripción no disponible"

#: app/SettingsBase.cpp:56
#, kde-format
msgctxt "Search through a list of control modules"
msgid "Search"
msgstr "Buscar"

#: app/SettingsBase.cpp:149
#, kde-format
msgid "Highlight Changed Settings"
msgstr "Resaltar las preferencias modificadas"

#: app/SettingsBase.cpp:160
#, kde-format
msgid "Report a Bug in the Current Page…"
msgstr "Informar de errores en la página actual..."

#: app/SettingsBase.cpp:187
#, kde-format
msgid "Help"
msgstr "Ayuda"

#: app/sidebar/qml/CategoriesPage.qml:57
#, kde-format
msgid "Show intro page"
msgstr "Mostrar la página de introducción"

#: app/sidebar/qml/CategoriesPage.qml:120
#, kde-format
msgctxt "A search yielded no results"
msgid "No items matching your search"
msgstr "Ningún elemento satisface la búsqueda"

#: app/sidebar/qml/HamburgerMenuButton.qml:25
#, kde-format
msgid "Show menu"
msgstr "Mostrar el menú"

#: app/sidebar/qml/introPage.qml:55
#, kde-format
msgid "Plasma"
msgstr "Plasma"

#: app/sidebar/SidebarMode.cpp:628
#, kde-format
msgid "Sidebar"
msgstr "Barra lateral"

#: app/sidebar/SidebarMode.cpp:700
#, kde-format
msgid "Most Used"
msgstr "Más usados"

#. i18n: ectx: ToolBar (mainToolBar)
#: app/systemsettingsui.rc:15
#, kde-format
msgid "About System Settings"
msgstr "Acerca de las preferencias del sistema"

#: app/ToolTips/tooltipmanager.cpp:188
#, kde-format
msgid "Contains 1 item"
msgid_plural "Contains %1 items"
msgstr[0] "Contiene 1 elemento"
msgstr[1] "Contiene %1 elementos"

#: core/ExternalAppModule.cpp:25
#, kde-format
msgid "%1 is an external application and has been automatically launched"
msgstr "%1 es una aplicación externa y se ha lanzado automáticamente"

#: core/ExternalAppModule.cpp:26
#, kde-format
msgid "Relaunch %1"
msgstr "Volver a lanzar %1"

#. i18n: ectx: property (windowTitle), widget (QWidget, ExternalModule)
#: core/externalModule.ui:14
#, kde-format
msgid "Dialog"
msgstr "Diálogo"

#: core/ModuleView.cpp:176
#, kde-format
msgid "Reset all current changes to previous values"
msgstr "Reiniciar todos los cambios actuales con sus valores anteriores"

#: core/ModuleView.cpp:335
#, kde-format
msgid ""
"The settings of the current module have changed.\n"
"Do you want to apply the changes or discard them?"
msgstr ""
"Hay cambios sin guardar en el módulo activo.\n"
"¿Desea aplicar los cambios o descartarlos?"

#: core/ModuleView.cpp:340
#, kde-format
msgid "Apply Settings"
msgstr "Aplicar preferencias"

#: runner/systemsettingsrunner.cpp:37
#, kde-format
msgid "Finds system settings modules whose names or descriptions match :q:"
msgstr ""
"Encuentra módulos de preferencias del sistema cuyos nombres o descripciones "
"coincidan con :q:"

#: runner/systemsettingsrunner.cpp:111
#, kde-format
msgid "System Information"
msgstr "Información del sistema"

#~ msgid "About Active View"
#~ msgstr "Acerca de la vista activa"

#~ msgid "About %1"
#~ msgstr "Acerca de %1"

#~ msgid ""
#~ "System Settings was unable to find any views, and hence has nothing to "
#~ "display."
#~ msgstr ""
#~ "El sistema de configuración no ha podido encontrar ninguna vista, por lo "
#~ "que no hay nada que mostrar."

#~ msgid "No views found"
#~ msgstr "No se han encontrado vistas"

#~ msgid "Internal name for the view used"
#~ msgstr "Nombre usado internamente para la vista"

#~ msgid "Switch to Icon View"
#~ msgstr "Cambiar a la vista de iconos"

#~ msgid "Switch to Sidebar View"
#~ msgstr "Cambiar a la vista de barra lateral"

#~ msgid "Icon View"
#~ msgstr "Vista de iconos"

#~ msgid "Provides a categorized icons view of control modules."
#~ msgstr ""
#~ "Proporciona una vista de iconos por categorías de los módulos de control."

#~ msgid "All Settings"
#~ msgstr "Todas las preferencias"

#~ msgid "Keyboard Shortcut: %1"
#~ msgstr "Acceso rápido de teclado: %1"

#~ msgid "Sidebar View"
#~ msgstr "Vista de barra lateral"

#~ msgid "Provides a categorized sidebar for control modules."
#~ msgstr ""
#~ "Proporciona una barra lateral con categorías para módulos de control."

#~ msgid "(c) 2017, Marco Martin"
#~ msgstr "© 2017, Marco Martin"

#~ msgid "<i>Contains 1 item</i>"
#~ msgid_plural "<i>Contains %1 items</i>"
#~ msgstr[0] "<i>Contiene 1 elemento</i>"
#~ msgstr[1] "<i>Contiene %1 elementos</i>"

#~ msgid "Most used module number %1"
#~ msgstr "Módulo número %1 más usado"

#~ msgid "Frequently Used"
#~ msgstr "Usadas frecuentemente"

#~ msgid "Go back"
#~ msgstr "Volver"

#~ msgid "View Style"
#~ msgstr "Estilo de la vista"

#~ msgid "Show detailed tooltips"
#~ msgstr "Mostrar consejos detallados"

#~ msgid "Configure…"
#~ msgstr "Configurar..."

#~ msgctxt "General config for System Settings"
#~ msgid "General"
#~ msgstr "General"

#~ msgid ""
#~ "System Settings was unable to find any views, and hence nothing is "
#~ "available to configure."
#~ msgstr ""
#~ "El sistema de configuración no ha podido encontrar ninguna vista, por lo "
#~ "que no hay nada disponible para configurar."

#~ msgid "Determines whether detailed tooltips should be used"
#~ msgstr "Determina si se deben usar consejos detallados"

#~ msgctxt "Action to show indicators for settings with custom data"
#~ msgid "Highlight Changed Settings"
#~ msgstr "Resaltar las preferencias modificadas"

#~ msgid "About Active Module"
#~ msgstr "Acerca del módulo activo"

#~ msgid "Back"
#~ msgstr "Volver"

#~ msgid "Configure your system"
#~ msgstr "Configurar el sistema"

#~ msgid ""
#~ "Welcome to \"System Settings\", a central place to configure your "
#~ "computer system."
#~ msgstr ""
#~ "Bienvenido a las «Preferencias del sistema», un lugar donde podrá "
#~ "configurar su equipo informático."

#~ msgid "Tree View"
#~ msgstr "Vista de árbol"

#~ msgid "Provides a classic tree-based view of control modules."
#~ msgstr "Proporciona una vista de árbol clásica de los módulos de control."

#~ msgid "Expand the first level automatically"
#~ msgstr "Expandir el primer nivel automáticamente"

#~ msgid "Search..."
#~ msgstr "Buscar..."

#~ msgid "System Settings Handbook"
#~ msgstr "Manual de las preferencias del sistema"

#~ msgid "About KDE"
#~ msgstr "Acerca de KDE"

#~ msgid "Select an item from the list to see the available options"
#~ msgstr ""
#~ "Seleccione un elemento de la lista para ver las opciones disponibles"

#~ msgid "Overview"
#~ msgstr "Vista general"
